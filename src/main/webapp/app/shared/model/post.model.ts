export interface IPost {
    id?: number;
    title?: string;
    body?: any;
    imageContentType?: string;
    image?: any;
}

export class Post implements IPost {
    constructor(public id?: number, public title?: string, public body?: any, public imageContentType?: string, public image?: any) {}
}
